import java.util.List;
import java.util.Scanner;
import java.util.InputMismatchException;

// Clase Doctor.
public class Doctor {
    private String nombreDoctor;
    private String domicilio;
    private String especialidad;
    private Integer idDoctor;
    private static int cont = 1;
    Scanner scn = new Scanner(System.in);

    // Constructor vacio.
    public Doctor(){
        
    }

    // Sobrecarga de metodos.
    // Segundo constructor.
    public Doctor(String nombreDoctor, String domicilio, String especialidad, Integer idDoctor){
        this.nombreDoctor = nombreDoctor;
        this.domicilio = domicilio;
        this.especialidad = especialidad;
        this.idDoctor = cont;
        cont++;
    }

    // Setters y getters.
    public String getNombreDoctor() {
        return nombreDoctor;
    }
    public void setNombreDoctor(String nombreDoctor) {
        this.nombreDoctor = nombreDoctor;
    }
    public String getDomicilio() {
        return domicilio;
    }
    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }
    public String getEspecialidad() {
        return especialidad;
    }
    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }
    public Integer getIdDoctor() {
        return idDoctor;
    }
    public void setIdDoctor(Integer idDoctor) {
        this.idDoctor = idDoctor;
    }
    public static int getCont() {
        return cont;
    }
    public static void setCont(int cont) {
        Doctor.cont = cont;
    }

    // Metodo para dar de alta Doctores.
    public void altaDoctor(List <Doctor> listaDoctores, List <Unidad> listUnidad){
        
        System.out.println("Ingresa el nombre completo del doctor: ");
        nombreDoctor = scn.nextLine();
        System.out.println("Ingresa la especialidad del doctor: ");
        especialidad = scn.nextLine();
        System.out.println("Ingresa el domicilio del doctor: ");
        domicilio = scn.nextLine();
        scn.nextLine();
        listaDoctores.add(new Doctor(nombreDoctor, domicilio, especialidad, idDoctor));

        menuDoctor(listaDoctores, listUnidad);
    }

    // Metodo para dar de baja Doctores.
    public void bajaDoctor(List <Doctor> listaDoctores, List <Unidad> listUnidad){
        int x, confirm;

        try {
            System.out.println("Ingresa el ID del doctor que deseas eliminar");
            x = scn.nextInt();
        
            System.out.println("\n" + "¿Seguro que deseas eliminar esta cuenta?" + "\n" + "1. Si" + "\n" + "2. No" + "\n");
            System.out.println(listaDoctores.get(x - 1));
            confirm = scn.nextInt();
            scn.nextLine();

            if (confirm == 1) {
                listaDoctores.remove(x - 1);
                System.out.println("Se dió de baja de forma correcta." + "\n");
            } else {
                System.out.println("No se dió de baja al doctor." + "\n");
        }

        menuDoctor(listaDoctores, listUnidad);
        } catch (InputMismatchException e) {
            System.out.println("Solo se aceptan números, intentalo nuevamente.");
            bajaDoctor(listaDoctores, listUnidad);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("El ID que ingresaste no existe o es incorrecto, intentalo nuevamente.");
            bajaDoctor(listaDoctores, listUnidad);
        }
    }

    // Metodo para consultar lista de Doctores.
    public void consulta(List <Doctor> listaDoctores, List <Unidad> listUnidad){
        int op;
        
        try {
            System.out.println("Elíge la opción que deseas consultar: " + "\n" + "1. Consultar lista completa de doctores" + "\n" + "2. Consulta individual" + "\n");
            op = scn.nextInt();

            switch (op) {
                case 1:
                    System.out.println("----- Lista de Doctores -----");
                    for (Doctor doctor : listaDoctores) {
                        System.out.println(doctor);
                    }
                    System.out.println("-----------------------------" + "\n");
                    break;
                case 2:
                    int idC;

                    System.out.println("Ingresa el ID del doctor que deseas consultar: ");
                    idC = scn.nextInt();

                    System.out.println("-----------------------------");
                    System.out.println(listaDoctores.get(idC - 1));
                    
                    System.out.println("-----------------------------" + "\n");
                    break;
            }

            menuDoctor(listaDoctores, listUnidad);
        } catch (InputMismatchException e) {
            System.out.println("Solo se pueden ingresar numeros, intentalo nuevamente." + "\n");
            consulta(listaDoctores, listUnidad);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("El ID que ingresaste es incorrecto o no existe, intentalo nuevamente." + "\n");
            consulta(listaDoctores, listUnidad);
        }
    }

    // Metodo para modificar lista de Doctores.
    public void modLista(List <Doctor> listaDoctor, List <Unidad> listUnidad){
        int op, idMod;
        // idMod = 1;

        try {
            System.out.println("Ingresa el ID del doctor al que se le modificará la información: ");
            idMod = scn.nextInt();

            System.out.println("Elíge el campo que deseas modificar: " + "\n" + "1. Nombre del Doctor" + "\n" + "2. Domicilio del Doctor" + "\n" + "3. Especialidad del Doctor" + "\n");
            op = scn.nextInt();
            scn.nextLine();

            switch (op) {
                case 1:
                    String nombreMod;
                    System.out.println("----- Ingresa el nombre correcto -----");
                    nombreMod = scn.nextLine();
                    listaDoctor.get(idMod - 1).nombreDoctor = nombreMod;

                    System.out.println("Se modificó el nombre correctamente");
                    System.out.println(listaDoctor.get(idMod - 1));
                    
                    System.out.println("--------------------------------------");
                    break;
                case 2:
                    String domicilioMod;
                    System.out.println("----- Ingresa el domicilio correcto -----");
                    domicilioMod = scn.nextLine();
                    listaDoctor.get(idMod - 1).domicilio = domicilioMod;

                    System.out.println("Se modificó el domicilio correctamente");
                    System.out.println(listaDoctor.get(idMod - 1));
                    
                    System.out.println("--------------------------------------");
                    break;
                case 3:
                    String especialidadMod;
                    System.out.println("----- Ingresa la especialidad correcta -----");
                    especialidadMod = scn.nextLine();
                    listaDoctor.get(idMod - 1).especialidad = especialidadMod;

                    System.out.println("Se modificó la especialidad correctamente");
                    System.out.println(listaDoctor.get(idMod - 1));
                    
                    System.out.println("--------------------------------------");
                    break;
            }

            menuDoctor(listaDoctor, listUnidad);
        } catch (InputMismatchException e) {
            System.out.println("Solo se pueden ingresar numeros, intentalo nuevamente." + "\n");
            // modLista(listaDoctor);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("El ID que ingresaste es incorrecto o no existe, intentalo nuevamente." + "\n");
            // modLista(listaDoctor);
        }
    }

    // Metodo Menú Doctor.
    public void menuDoctor(List <Doctor> listaDoctores, List <Unidad> listUnidad){
        int op;
        boolean salir = true;

        try {
            System.out.println("Ingresa la operación que deseas realizar." + "\n" + "1. Alta doctor" + "\n" + "2. Baja doctor" + "\n" + "3. Consulta" + "\n" + "4. Modificar información" + "\n" + "5. Salir");
            op = scn.nextInt();
            scn.nextLine();

            while (salir) {
                switch (op) {
                    case 1:
                        
                        altaDoctor(listaDoctores, listUnidad);
                        break;
                    case 2:
                        bajaDoctor(listaDoctores, listUnidad);
                        break;
                    case 3:
                        consulta(listaDoctores, listUnidad);
                        break;
                    case 4:
                        modLista(listaDoctores, listUnidad);
                        break;
                    case 5:
                        salir = false;
                        break;
                    default:
                        System.out.println("\n" + "Solo números entre 1 y 5");
                        menuDoctor(listaDoctores, listUnidad);
                        break;
                }
                break;
            }
            
        } catch (InputMismatchException e) {
            System.out.println("Solo se pueden ingresar numeros, intentalo nuevamente." + "\n");
            menuDoctor(listaDoctores, listUnidad);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("El ID que ingresaste es incorrecto o no existe, intentalo nuevamente." + "\n");
            menuDoctor(listaDoctores, listUnidad);
        }
    }

    // Metodo toString sobreescrito.
    @Override
    public String toString(){
        return "\n" + "Nombre del Doctor: " + nombreDoctor + "\n" + "Domicilio del Doctor: " + domicilio + "\n" + "Especialidad del Doctor: " + especialidad + "\n" + "ID del Doctor: " + idDoctor + "\n";
    }

    
}
