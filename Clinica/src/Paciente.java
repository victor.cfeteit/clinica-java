

import java.util.List;
import java.util.Scanner;

public class Paciente{
    
// Atributos de mi clase 
    private Integer id;
    private String nombre;
    private Integer edad;
    private Integer telefono;
// Contador para asignar in id al paciente de forma automatica
    private static int contador = 1;

// Constructor con mis atributos 
    public Paciente(Integer id, String nombre, Integer edad, Integer telefono) {
        this.id = id;
        this.nombre = nombre;
        this.edad = edad;
        this.telefono = telefono;
        contador++;
    }

// Constructor vacio
    public Paciente(){

    }

// Setters y Getters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

// Metodo toString 
    public String toString(){
        return "---> ID: " + this.id + "\n" + "     Nombre: " + this.nombre + "\n" + "     Edad: " + this.edad + "\n" + "     Telefono: " + this.telefono; 
    }

// Metodo menu principal paciente
    public void menu(List<Paciente> listaPaciente, List <Unidad> listUnidad){
        Scanner entrada = new Scanner(System.in);
        System.out.println("|------------------------------|\n"
                        +  "|       Elija una opcion       |\n"
                        +  "|------------------------------|\n"
                        +  "|1. Alta Paciente              |\n"
                        +  "|2. Baja Paciente              |\n"
                        +  "|3. Consultar Paciente         |\n"
                        +  "|4. Modificar Datos(Paciente)  |\n"
                        +  "|5. Salir                      |\n"       
                        +  "--------------------------------");
                    
        Integer opcion = entrada.nextInt();

        switch (opcion) {
            case 1:
                altaPaciente(listaPaciente, listUnidad);
                break;
            
            case 2: 
                bajaPaciente(listaPaciente, listUnidad);
                break;
            
            case 3: 
                consultaPaciente(listaPaciente, listUnidad);
                break;

            case 4:
                modificacionPaciente(listaPaciente, listUnidad);
                break;

            case 5:
                Unidad unidadx = new Unidad();
                unidadx.menuUnidad(listUnidad);
                break;
        }
        entrada.close();
    }

//Metodo para ingresar datos y agregarlos a nuetra ArrayList
    public void altaPaciente(List<Paciente> listaPaciente,List <Unidad> listUnidad){
        Scanner alta = new Scanner(System.in);
        System.out.println("Ingresa el nombre: ");
        String nombre = alta.nextLine();
        System.out.println("Ingresa la edad: ");
        Integer edad = alta.nextInt();
        System.out.println("Ingresa el numero de telefono: ");
        Integer telefono = alta.nextInt();

        listaPaciente.add(new Paciente(contador, nombre, edad, telefono));

        System.out.println("");
        imprimir(listaPaciente);
        menu(listaPaciente,listUnidad);
        alta.close();
    }

// Metodo para eliminar pacientes del ArrayList
    public void bajaPaciente(List<Paciente> listaPaciente, List <Unidad> listUnidad){
        Scanner baja = new Scanner(System.in);
        System.out.println("Ingresa el id del paciente: ");
        Integer idBaja = baja.nextInt();
        System.out.println("");
        listaPaciente.remove(idBaja-1);

        System.out.println("");
        imprimir(listaPaciente);
        menu(listaPaciente, listUnidad);
        baja.close();
    }

// Metodo para consultar los datos del paciente
    public void consultaPaciente(List<Paciente> listaPaciente, List <Unidad> listUnidad){
        Scanner consulta = new Scanner(System.in);
        System.out.println("Ingresa el id del paciente: ");
        Integer idConsulta = consulta.nextInt();
        System.out.println("\n" + listaPaciente.get(idConsulta-1) + "\n");
        menu(listaPaciente, listUnidad);
        consulta.close();
    }

// Metodo para modificar los datos del paciente
    public void modificacionPaciente(List<Paciente> listaPaciente, List <Unidad> listUnidad){
        Scanner modificar = new Scanner(System.in);
        System.out.println("|------------------------|\n"
                        +  "| ¿Qué deseas modificar? |\n" 
                        +  "|------------------------|\n"
                        +  "|1. Nombre               |\n"
                        +  "|2. Edad                 |\n"
                        +  "|3. Telefono             |\n"
                        +  "--------------------------");

        Integer opcion = modificar.nextInt();
        System.out.println("Ingresa el id del paciente: ");
        Integer idMod = modificar.nextInt();
        modificar.nextLine();

            if(opcion == 1){
                System.out.println("Ingresa el nombre: ");
                String nom = modificar.nextLine();
                listaPaciente.get(idMod -1).nombre = nom;
                System.out.println("");
                imprimir(listaPaciente);
                System.out.println("");
                
            }else if(opcion == 2){
                System.out.println("Ingresa la edad");
                Integer edad2 = modificar.nextInt();
                listaPaciente.get(idMod -1).edad = edad2;
                System.out.println("");
                imprimir(listaPaciente);
                System.out.println("");

            }else if(opcion == 3){
                System.out.println("Ingresa el numero de telefono: ");
                Integer telefono2 = modificar.nextInt();
                listaPaciente.get(idMod -1).telefono = telefono2;
                System.out.println("");
                imprimir(listaPaciente);
                System.out.println("");
            }

        menu(listaPaciente, listUnidad);
        modificar.close();
    }

// Metodo para imprimir mi ArrayList
    public void imprimir(List<Paciente> listaPaciente){
       for (Paciente paciente : listaPaciente) {
        System.out.println(paciente.toString());
       }
    }


    

}