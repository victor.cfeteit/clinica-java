import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Unidad {
    private int identificador;
    private String nombre;
    private String planta;
    private List<Doctor> listaDoctores = new ArrayList<>();
    private List<Paciente> listaPaciente = new ArrayList<>();
    private static int contID;

    public Unidad(){}

    public Unidad(int identificador, String nombre, String planta, List<Doctor> listaDoctores,List<Paciente> listaPaciente ) {
        this.identificador = identificador;
        this.nombre = nombre;
        this.planta = planta;
        this.listaDoctores = listaDoctores;
        this.listaPaciente = listaPaciente;
        contID++;
    }
    public int getIdentificador() {
        return identificador;
    }
    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getPlanta() {
        return planta;
    }
    public void setPlanta(String planta) {
        this.planta = planta;
    }
    public List<Doctor> getDoctor() {
        return listaDoctores;
    }
    public void setDoctor(List<Doctor> listaDoctores) {
        this.listaDoctores = listaDoctores;
    }

    @Override
    public String toString() {
        return "\n\t-----Unidad-----\n " + "\n---> Nombre: " + nombre + "\n---> Planta: " + planta + "\n---> Doctor: "
                + listaDoctores + "\n--->ID: " + identificador + "\n--> Pacientes: " + listaPaciente ;
    } 

    public void altaUnidad(List <Unidad> listUnidad, List<Doctor> listaDoctores, List<Paciente> listaPaciente){
        Doctor doctor1 = new Doctor();
        Paciente paciente1 = new Paciente();
        Scanner ingresa = new Scanner(System.in);
        System.out.println("Ingresa los datos de la Unidad: ");
        System.out.println("Nombre de la Unidad: ");
        String nombreUnidad = ingresa.nextLine();
        System.out.println("¿A que planta pertenece?");
        String plantaP = ingresa.nextLine();
        listUnidad.add(new Unidad(identificador, nombreUnidad, plantaP, listaDoctores, listaPaciente));
        doctor1.menuDoctor(listaDoctores, listUnidad);
        paciente1.menu(listaPaciente,listUnidad);
        menuUnidad(listUnidad);
    }

    public void bajaUnidad(List <Unidad> listUnidad, int eliminar){
        Scanner ingresa = new Scanner(System.in);
        System.out.println("¿Seguro que quieres eliminar esta Unidad de la Clinica?");
        System.out.println(listUnidad.get(eliminar));
        System.out.print("\t --> 1. SI\n"
                        +"\t --> 2. NO\n"
                        +"--> Ingresa la opción: ");
        int siYno = ingresa.nextInt();
        if(siYno == 1){
            listUnidad.remove(eliminar);
            menuUnidad(listUnidad);
        }else if(siYno == 2){
            menuUnidad(listUnidad);
        }else if (siYno != 1 || siYno != 2){
            System.out.println("Ingrese un valor valido");
            bajaUnidad(listUnidad, eliminar);
        } 
    }

    public void consultaUnidad(List <Unidad> listUnidad){
        Scanner ingresa = new Scanner(System.in);
        System.out.println("¿Quieres revisar todas las unidades de la Clinica?");
        System.out.print("\t --> 1. SI\n"
                        +"\t --> 2. NO\n"
                        +"--> Ingresa la opción: ");
        try {
            int siYno = ingresa.nextInt();
            if(siYno == 1){
                for(Unidad x:listUnidad) {
                    System.out.println(x);
                }
                menuUnidad(listUnidad);   
            }else if(siYno == 2){
                System.out.println("¿Que unidad quieres revisar?" );
                
                int consulta = ingresa.nextInt();
                System.out.println(listUnidad.get(consulta));
                menuUnidad(listUnidad);  
            }else if (siYno != 1 || siYno != 2){
                System.out.println("Ingrese una opción valida");
                consultaUnidad(listUnidad);
            }
        }catch (InputMismatchException inputMismatchException) {
            System.out.println("\n\t----- ERROR ESCRIBA UN VALOR VALIDO -----\n");
            consultaUnidad(listUnidad);
        }catch (IndexOutOfBoundsException indexOutOfBoundsException) {
            System.out.println("\n\t----- ERROR INDICA UN ID EXISTENTE -----\n");
            consultaUnidad(listUnidad);
        }  
    }

    public void modificacionUnidad(List <Unidad> listUnidad){
        Scanner ingresa = new Scanner(System.in);
        try { 
            System.out.print("\t------Valor a modificar------\n"
                            +"\t1. Nombre\n"
                            +"\t2. Planta\n"
                            +"\t3. Modificar Doctor"
                            +"\t4. Salir\n"
                            +"\t--> Ingresa la opción: ");
            int opcion = ingresa.nextInt();
            ingresa.nextLine();
            if(opcion > 4 || opcion < 1){
                System.out.println("\n\t----- ELIGE UNA OPCION DEL 1 AL 5 -----\n");
                modificacionUnidad(listUnidad);
            }                   
            switch(opcion){
                case 1:
                    System.out.println("¿Que Unidad quieres modificar?");
                    int unidadModN = ingresa.nextInt();
                    ingresa.nextLine();
                    System.out.println("El nombre actual de la clinica es: " + listUnidad.get(unidadModN).nombre);
                    System.out.print("--> Ingresa el nuevo nombre: ");
                    String modNombre = ingresa.nextLine();
                    listUnidad.get(unidadModN).nombre=modNombre;
                    System.out.println(listUnidad.get(unidadModN));
                    modificacionUnidad(listUnidad);
                break;
                case 2:
                    System.out.println("¿Que Unidad quieres modificar?");
                    int unidadModP = ingresa.nextInt();
                    ingresa.nextLine();
                    System.out.println("La planta actual es "+ listUnidad.get(unidadModP).planta);    
                    System.out.print("--> Agrega el cambio de planta: ");
                    String modModelo = ingresa.nextLine();
                    listUnidad.get(unidadModP).planta=modModelo;
                    System.out.println(listUnidad.get(unidadModP));
                    modificacionUnidad(listUnidad);
                break;
                case 3:
                    Doctor doctor2 = new Doctor();
                    System.out.println("¿Que unidad quieres modificar?");
                    int unidadModD = ingresa.nextInt();
                    
                    System.out.println("El doctor actual es " + listUnidad.get(unidadModD).listaDoctores);
                    System.out.println("--> Agrega la modificacion del doctor: ");
                    // List <Doctor> listaDoctores = new ArrayList<Doctor>();
                    doctor2.modLista(listaDoctores, listUnidad);
                    listUnidad.get(unidadModD).setDoctor(listaDoctores);
                    
                    
                    
                    modificacionUnidad(listUnidad);
                    ingresa.nextLine();
                break;
                case 4:
                menuUnidad(listUnidad);
                break;
                
            }
        } catch (InputMismatchException inputMismatchException ) {
            System.out.println("\n\t----- ERROR ESCRIBA UN VALOR VALIDO -----\n");
            modificacionUnidad(listUnidad);
        }catch (IndexOutOfBoundsException indexOutOfBoundsException) {
            System.out.println("\n\t----- ERROR INDICA UN ID EXISTENTE -----\n");
            modificacionUnidad(listUnidad);
        }
    }

    public void menuUnidad(List <Unidad> listUnidad){
        System.out.print("  ------ Menú ------\n"
                        +"  1. Alta Unidad\n"
                        +"  2. Baja Unidad\n"
                        +"  3. Consulta\n"
                        +"  4. Modificación\n"
                        +"  5. Salir\n"
                        +"  --> Ingresa la opción: ");
        Scanner ingresa = new Scanner(System.in);
        try {
            int opcion = ingresa.nextInt();
            ingresa.nextLine();
            if(opcion > 5 || opcion < 1){
                System.out.println("\n\t----- ELIGE UNA OPCION DEL 1 AL 5 -----\n");
                menuUnidad(listUnidad);
            }  
            switch(opcion){  
            case 1:
                List<Paciente> listaPaciente = new ArrayList<>();
                List<Doctor> listaDoctores = new ArrayList<Doctor>();
                altaUnidad(listUnidad, listaDoctores,listaPaciente);
            break; 
            case 2:
                System.out.println("¿Que Unidad quieres eliminar?");
                int eliminar = ingresa.nextInt();
                bajaUnidad(listUnidad, eliminar); 
            break;
            case 3:
                consultaUnidad(listUnidad);
            break;
            case 4:
                modificacionUnidad(listUnidad);
            break;
            case 5: 
            break;

            }
        } catch (InputMismatchException inputMismatchException) {
            System.out.println("\n\t----- ERROR ESCRIBA UN VALOR VALIDO -----\n");
            menuUnidad(listUnidad);
        }catch (IndexOutOfBoundsException indexOutOfBoundsException) {
            System.out.println("\n\t----- ERROR INDICA UN ID EXISTENTE -----\n");
            menuUnidad(listUnidad);
        }
    }

}